package it.pegaso.libroOCd.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.libriOCd.model.LibroCd;

public class LibroCdDao {
	
	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/libroocd?user=root&password=admin";
	
	private Connection connection;
	private PreparedStatement getLibroById;
	private PreparedStatement getCdById;
	
	public LibroCd getLibro(int id) {
		LibroCd result = null;
		
		try {
			getGetLibroById().clearParameters();
			
			getGetLibroById().setInt(1, id);
			ResultSet rs = getGetLibroById().executeQuery();
			
			if(rs.next()) {
				result= new LibroCd();
				result.setId(rs.getInt("id"));
				result.setTitolo(rs.getString("titolo"));
				result.setAutore(rs.getString("autore"));
				result.setGenere(rs.getString("genere"));
				result.setAnno(rs.getInt("Anno"));
				result.setIsLibroOrCd(rs.getInt("isLibroOrCd"));
				result.setQuantita(rs.getInt("quantita"));
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public LibroCd getCd(int id) {
		LibroCd result = null;
		
		try {
			getGetCdById().clearParameters();
			
			getGetCdById().setInt(1, id);
			ResultSet rs = getGetCdById().executeQuery();
			
			if(rs.next()) {
				result= new LibroCd();
				result.setId(rs.getInt("id"));
				result.setTitolo(rs.getString("titolo"));
				result.setAutore(rs.getString("autore"));
				result.setGenere(rs.getString("genere"));
				result.setAnno(rs.getInt("Anno"));
				result.setIsLibroOrCd(rs.getInt("isLibroOrCd"));
				result.setQuantita(rs.getInt("quantita"));
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<LibroCd> findAll(){
		
		List<LibroCd> result = new ArrayList<LibroCd>();
		
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("Select * from campionario");
			
			while(rs.next()) {
				LibroCd l= new LibroCd();
				l.setId(rs.getInt("id"));
				l.setTitolo(rs.getString("titolo"));
				l.setAutore(rs.getString("autore"));
				l.setGenere(rs.getString("genere"));
				l.setAnno(rs.getInt("Anno"));
				l.setIsLibroOrCd(rs.getInt("isLibroOrCd"));
				l.setQuantita(rs.getInt("quantita"));
				result.add(l);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	protected Connection getConnection() throws SQLException, ClassNotFoundException {
		
		if (connection== null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection =DriverManager.getConnection(CONNECTION_STRING); 
		}
		return connection;
	}
	protected void setConnection(Connection connection) {
		this.connection = connection;
	}
	protected PreparedStatement getGetLibroById() throws ClassNotFoundException, SQLException {
		if (getLibroById == null) {
			getLibroById = getConnection().prepareStatement("Select * from campionario where id=? and isLibroOrCd=0");
		}
		return getLibroById;
	}
	protected void setGetLibroById(PreparedStatement getLibroById) {
		this.getLibroById = getLibroById;
	}
	protected PreparedStatement getGetCdById() throws ClassNotFoundException, SQLException {
		if (getCdById == null) {
			getCdById = getConnection().prepareStatement("Select * from campionario where id=? and isLibroOrCd=1");
		}
		return getCdById;
	}
	protected void setGetCdById(PreparedStatement getCdById) {
		this.getCdById = getCdById;
	}

	
	
	

}
