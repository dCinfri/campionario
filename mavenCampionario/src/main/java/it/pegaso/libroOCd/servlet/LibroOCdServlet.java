package it.pegaso.libroOCd.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.libriOCd.model.LibroCd;
import it.pegaso.libroOCd.dao.LibroCdDao;

/**
 * Servlet implementation class LibroOCdServlet
 */
public class LibroOCdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LibroOCdServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<LibroCd> list = new ArrayList<LibroCd>();
		LibroCdDao l = new LibroCdDao();
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title> Collezione Maledetta </title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<table border=3><tbody>");
		list = l.findAll();
		for (LibroCd c : list) {
			out.println("<tr>");
			out.println("<td>");
			out.println("<h2>&#129493;");
			out.println(c.toString());
			out.println("&#128115;</h2>");
			out.println("&nbsp;</td>");
			out.println("</tr>");
		}
		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
