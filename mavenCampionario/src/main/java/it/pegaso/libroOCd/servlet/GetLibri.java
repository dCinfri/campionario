package it.pegaso.libroOCd.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.libriOCd.model.LibroCd;
import it.pegaso.libroOCd.dao.LibroCdDao;

/**
 * Servlet implementation class GetLibri
 */
public class GetLibri extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetLibri() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		LibroCdDao dao = new LibroCdDao();
		List<LibroCd> result = new ArrayList<LibroCd>();
		
		result = dao.findAll();
		
		request.setAttribute("list", result);
		
		RequestDispatcher rs = request.getRequestDispatcher("showLibri.jsp");
		
		rs.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
