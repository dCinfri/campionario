package it.pegaso.libriOCd.model;

public class LibroCd {
	
	private int id;
	private String titolo;
	private String autore;
	private String genere;
	private int anno;
	private int isLibroOrCd;
	private int quantita;
	
	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTitolo() {
		return titolo;
	}



	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}



	public void setAutore(String autore) {
		this.autore = autore;
	}



	public String getGenere() {
		return genere;
	}



	public void setGenere(String genere) {
		this.genere = genere;
	}



	public int getAnno() {
		return anno;
	}



	public void setAnno(int anno) {
		this.anno = anno;
	}



	public int getIsLibroOrCd() {
		return isLibroOrCd;
	}



	public void setIsLibroOrCd(int isLibroOrCd) {
		this.isLibroOrCd = isLibroOrCd;
	}



	public int getQuantita() {
		return quantita;
	}



	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}



	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		if(isLibroOrCd == 0) {
			sb.append("Libro: ");
		}
		else {
			sb.append("Cd: ");
		}
		sb.append(getTitolo());
		sb.append(" by ");
		sb.append(getAutore());
		sb.append(" Genere: ");
		sb.append(getGenere());
		
		return sb.toString();
	}
	
	

}
