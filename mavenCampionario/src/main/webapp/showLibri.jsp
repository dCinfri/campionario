<%@page import="it.pegaso.libriOCd.model.LibroCd"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogo Libri</title>
</head>
<body>
     <h1 style="color: red; text-align: center; font-weight: bold; ">Ecco i nostri libri</h1>
     
     <table align="center" > 
       <thead>
          <tr>
               <th style="width: 20cm; border: 1px solid black">Titolo</th>
               <th style="width: 20cm; border: 1px solid black">Autore</th> 
               <th style="width: 20cm; border: 1px solid black">Genere</th>
         </tr>
       </thead>
       
       <tbody>
            <% List<LibroCd> list = (List<LibroCd>)request.getAttribute("list");%>
            
            
            <%
                for(LibroCd lcd: list){
                
                	if(lcd.getIsLibroOrCd() == 0){ 
                		
                		 %> 		
                		<tr>
                        <td style = "width: 20cm; border: 1px solid black; text-align: center;"><%= lcd.getTitolo() %></td>
                        <td style = "width: 20cm; border: 1px solid black; text-align: center;"><%= lcd.getAutore() %></td>
                        <td style = "width: 20cm; border: 1px solid black; text-align: center;"><%= lcd.getGenere() %></td>
                        </tr>
              <%   		
                	}
                }
            
            %>
                
       
       
       </tbody>  
     </table>
        
 <a href="http://localhost:8080/mavenCampionario/SezioneClienti.jsp"><input type="button" value="Indietro"></a>       
</body>
</html>